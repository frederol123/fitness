$(window).on('load',function(){
    if (typeof tns === "function"){
        //Карусель ожиданий
        var expectationsCarousel = tns({
          container: '.expectations-card-slider',
          items: 1,
          swipeAngle: 40,
          speed: 800,
          mouseDrag: true,
          controls: false,
          navPosition: 'bottom',
          edgePadding: 10,
          responsive: {
            769: {
              nav: false,
              items: 3
            },
            480: {
              edgePadding: 40,
            }
          }
        });

        //Карусель отзывов
        var reviewsCarousel = tns({
            container: '.reviews-carousel',
            items: 1,
            arrowKeys: true,
            swipeAngle: 40,
            speed: 800,
            mouseDrag: true,
            autoplay: true,
            controls: false,
            navPosition: 'bottom',
            autoplayHoverPause: true,
            autoplayButtonOutput: false
        });

        //Карусель уровней
        var levelBtnPrev = document.querySelector('.levels__btns-control-prev');
        var levelBtnNext = document.querySelector('.levels__btns-control-next');
        var levelsBtns = tns({
        container: '.levels__btns-carousel',
        items: 1,
        swipeAngle: 40,
        speed: 1600,
        autoplay: false,
        autoplayButtonOutput: false,
        mouseDrag: true,
        controls: false,
        nav: false,
        responsive: {
            601: {
            items: 5
            }
        }
        });
        levelBtnPrev.addEventListener('click', function(){
            levelsBtns.goTo('prev');
        });
        levelBtnNext.addEventListener('click', function(){
            levelsBtns.goTo('next');
        });
        levelsBtns.events.on('indexChanged', function() {
            var currentSlide = document.querySelector('.levels__btns-carousel .tns-slide-active');
            var slideBtn = currentSlide.querySelector('button');

            slideBtn.click();
        });

        function changeTab(el){
            event.preventDefault();
          
            var activeTabLink = document.querySelector('[data-tab-link].active');
            var activeTabContent = document.querySelector('[data-tab-content].show-tab');
            var selectedTabLink = el;
            var selectedTabContent = document.querySelector('[data-tab-content="' + el.getAttribute('data-tab-link') + '"]');
          
            activeTabLink.classList.remove('active');
            activeTabContent.classList.remove('show-tab');
          
            selectedTabLink.classList.add('active');
            selectedTabContent.classList.add('show-tab');
          }

        //Смена таба
        var tabLinks = document.querySelectorAll('[data-tab-link]');
        for (var i = 0; i < tabLinks.length; ++i) {
            tabLinks[i].addEventListener('click', function(event){
            changeTab(this);
            });
        }
    }

    //Плавная прокрутка якорей
    var anchorLinks = document.querySelectorAll('a[href^="#"]');
    for (var i = 0; i < anchorLinks.length; ++i) {
        anchorLinks[i].addEventListener('click', function(event){
        event.preventDefault();

        var currentElement = document.querySelector(this.getAttribute('href'));
        if (currentElement){
            currentElement.scrollIntoView({
            behavior: 'smooth'
            });
        }
            
        });
    }

});

