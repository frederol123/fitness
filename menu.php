<header>
      <div class="header-wrap">
        <div class="wrap-logo">
          <a class="logo" href="/">
            <img src="/assets/img/heat.svg" alt="HEAT">
          </a>
        </div>
        
        <nav class="nav-wrap">
          <ul class="nav__list">
            <li class="nav__item">
              <a class="nav__link" href="/pages/about.php">О&nbsp;ПРОЕКТЕ</a>
            </li>
            <li class="nav__item">
              <a class="nav__link" href="#levels">УРОВНИ</a>
            </li>
            <li class="nav__item">
              <a class="nav__link" href="#reviews">ОТЗЫВЫ</a>
            </li>
            <li class="nav__item">
              <a class="nav__link" href="FAQ.html">ВОПРОСЫ И&nbsp;ОТВЕТЫ</a>
            </li>
          </ul>
        </nav>
        <div class="wrap-login-btn">
          <div class="login-btn" id="loginBtn" data-popup="open" data-id="logIn">
            <div class="login-btn__icon">
              <img src="/assets/img/login.png" alt="icon">
            </div>
            <div class="login-btn__text">Вход</div>
          </div>
        </div>

        <div class="toggle-menu">
          <div class="toggle-menu__btn">
            <span class="toggle-menu__line toggle-menu__line_top"></span>
            <span class="toggle-menu__line toggle-menu__line_middle"></span>
            <span class="toggle-menu__line toggle-menu__line_bottom"></span>
          </div>
        </div>

        <div class="header-dropdown-menu">
          <div class="header-dropdown-menu__link-wrap">
            <a class="header-dropdown-menu__link" href="project.html">О&nbsp;ПРОЕКТЕ</a>
          </div>
          <div class="header-dropdown-menu__link-wrap">
            <a class="header-dropdown-menu__link" href="#levels">УРОВНИ</a>
          </div>
          <div class="header-dropdown-menu__link-wrap">
            <a class="header-dropdown-menu__link" href="#reviews">ОТЗЫВЫ</a>
          </div>
          <div class="header-dropdown-menu__link-wrap">
            <a class="header-dropdown-menu__link" href="FAQ.html">ВОПРОСЫ И&nbsp;ОТВЕТЫ</a>
          </div>
        </div>
          </div>

</header>