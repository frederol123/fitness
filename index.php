<?php
  include('header.php');
  include('menu.php');
?>
<!-- # first-screen -->
<section id="first-screen">
  <div class="block">
    <div class="first-screen-content">
      <h1 class="head">
        Путь к <span>твоему</span> идеальному телу
      </h1>
      <div class="text">
      Авторский фитнес-курс Галины Огневой для дома и зала. Будь в форме не к лету, а навсегда!
      </div>
        <button class="content-button">Выбрать уровень</button>
    </div>
    <div class="first-screen-bg">
      <img src="/assets/img/f_screen_bg1.png" alt="">
    </div>
    <div class="first-screen-icon">
      <span class="decor"></span>
      <img class="icon" src="/assets/img/f_screen_icon1.png" alt="img">
    </div>
    
  </div>
</section>
<!-- # end first-screen -->

<!-- # second-screen -->
<section id="terms">
      <div class="terms">
        <h2 class="terms__head">ЭТОТ КУРС ДЛЯ тебя, <span class="terms__head-span">если:</span></h2>
        <div class="terms__inner">
          <div class="wrap-terms-circles">
            <div class="terms-circles">
              <span class="terms-circles__item">
                <div class="arrow">
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                </div>
                <span class="terms-circles__circle">1 </span>
              </span>
              <span class="terms-circles__item">
                <div class="arrow">
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                </div>
                <span class="terms-circles__circle">2</span>
              </span>
              <span class="terms-circles__item">
                <div class="arrow">
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                  <span class="arrow__span"></span>
                </div>
                <span class="terms-circles__circle">3</span>
              </span>
              <span class="terms-circles__item">
                <span class="terms-circles__circle">4</span>
              </span>
            </div>
          </div>

          <ul class="terms__list">
            <li class="terms__item">
              <p class="terms__text">Ты&nbsp;много раз пробовала меняться, но&nbsp;не&nbsp;добивалась <b>желаемого</b> результата, или не&nbsp;могла его <b>удержать</b></p>
            </li>
            <li class="terms__item">
              <p class="terms__text">Ты&nbsp;только начинаешь и&nbsp;хочешь получить <b>готовую инструкцию</b> к&nbsp;действию</p>
            </li>
            <li class="terms__item">
              <p class="terms__text">Ты&nbsp;<b>устала от&nbsp;диет</b> и&nbsp;не&nbsp;хочешь отказываться от&nbsp;любимой еды</p>
            </li>
            <li class="terms__item">
              <p class="terms__text">Тебе нужны <b>постоянная поддержка</b> и&nbsp;мотивация</p>
            </li>
          </ul>
        </div>
      </div>
  </section>

  <section id="expectations">
    <div class="container">
      <div class="expectations">
        <h2 class="expectations__head">Что тебя <span>ждет:</span></h2>
      
        <div class="expectations-cards">
          <div class="expectations-card-slider">
            <div class="card">
              <div class="card-wrap-img">
                <span class="card-wrap-img__circle"></span>
                <img class="card-wrap-img__img card__img" src="/assets/img/card-img-1.png" alt="img-card">
              </div>
              <div class="card__body card__body-border-red">
                <h6 class="card__head">Тренировки всегда&nbsp;под рукой</h6>
                <ul class="card__list">
                  <li class="card__item">
                    <p class="card__text"><strong>Круглосуточный доступ к&nbsp;личному кабинету&nbsp;&mdash; </strong>тренируйся в&nbsp;удобное время;</p>
                  </li>
                  <li class="card__item">
                    <p class="card__text"><strong>Удобный Вертикальный формат видео.</strong> Занимайся в&nbsp;любом месте&nbsp;&mdash; ты&nbsp;сможешь рассмотреть детали c&nbsp;любого устройства; </p>
                  </li>
                  <li class="card__item">
                    <p class="card__text"><strong>Пять уровней сложности.</strong><br>
                    Работай по&nbsp;подходящей именно ТЕБЕ программе;</p>
                  </li>
                  <li class="card__item">
                    <p class="card__text"><strong>Учись на&nbsp;моих ошибках!</strong><br>
                    Подробный видео-разбор техники и&nbsp;ошибок позволит тренироваться безопасно и&nbsp;эффективно;</p>
                  </li>
                </ul>
              </div>
            </div>

            <div class="card">
              <div class="card-wrap-img">
                <span class="card-wrap-img__circle"></span>
                <img class="card-wrap-img__img card__img" src="/assets/img/card-img-2.png" alt="img-card">
              </div>
              <div class="card__body card__body-border-white">
                <h6 class="card__head">Питание без запретов</h6>
                <ul class="card__list">
                  <li class="card__item">
                    <p class="card__text"><strong>Никаких готовых рационов!</strong><br>
                    Настрой питание под свой образ жизни. Выбирай сама, что, сколько и&nbsp;когда есть;</p>
                  </li>
                  <li class="card__item">
                    <p class="card__text"><strong>Узнай рабочие способы организации питания.</strong><br>
                    Тебя ждут простые инструкции и&nbsp;полезные рецепты с&nbsp;точным БЖУ и&nbsp;удобной навигацией;</p>
                  </li>
                </ul>
              </div>
            </div>

            <div class="card">
              <div class="card-wrap-img">
                <span class="card-wrap-img__circle"></span>
                <img class="card-wrap-img__img card__img" src="/assets/img/card-img-3.png" alt="img-card">
              </div>
              <div class="card__body card__body-border-red">
                <h6 class="card__head">Поддержка 24/7</h6>
                  <ul class="card__list">
                    <li class="card__item">
                      <p class="card__text"><strong>Будь в&nbsp;теме!</strong><br>
                      Регулярно участвуй в&nbsp;прямых эфирах со&nbsp;мной, психологом, и&nbsp;приглашенными экспертами;</p>
                    </li>
                    <li class="card__item">
                      <p class="card__text"><strong>Мы&nbsp;всегда рядом!</strong><br>
                      Делись своими мыслями и&nbsp;переживания в&nbsp;чате со&nbsp;мной и&nbsp;девочками, или сама стань примером для других;</p>
                    </li>
                    <li class="card__item">
                      <p class="card__text"><strong>Нет &laquo;неправильных&raquo; вопросов!</strong> Получай оперативную обратную связь, ни&nbsp;один твой вопрос не&nbsp;останется незамеченным;</p>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="decor-wrap">
          <img class="decor-img" src="/assets/img/f_screen_icon1.png" alt="img">
          <span class="decor-span"></span>
          <img class="decor-img-2" src="/assets/img/f_screen_icon1.png" alt="img">
          <span class="decor-span-2"></span>
        </div>
      </div>
    </div>
  </section>


  <?php
  #Заглушка для уровней тренировок
  $trainingLevels = [
    [
      'id' => 'beginner',
      'name' => 'Новичок',
      'price' => '2&nbsp;990 рублей',
      'description' => 'Первый этап на&nbsp;пути к&nbsp;твоему идеальному телу.<br>Изучаем и&nbsp;отрабатываем основные базовые движения.',
      'experience' => 'Опыт тренировок: Отсутствует или менее 6&nbsp;мес.',
      'about' => [
        'Сформируем базовые навыки работы со&nbsp;своим телом;',
        'Начнем налаживать питание;',
        'Подготовимся к&nbsp;работе со&nbsp;свободными весами;'
      ],
      'time' => '7 недель',
      'numberOfTrainings' => '12',
      'trainingDeclension' => 'тренировок'
    ],
    [
      'id' => 'beginner',
      'name' => 'Базовый',
      'price' => '2&nbsp;990 рублей',
      'description' => 'Первый этап на&nbsp;пути к&nbsp;твоему идеальному телу.<br>Изучаем и&nbsp;отрабатываем основные базовые движения.',
      'experience' => 'Опыт тренировок: Отсутствует или менее 6&nbsp;мес.',
      'about' => [
        'Сформируем базовые навыки работы со&nbsp;своим телом;',
        'Начнем налаживать питание;',
        'Подготовимся к&nbsp;работе со&nbsp;свободными весами;'
      ],
      'time' => '3 недели',
      'numberOfTrainings' => '7',
      'trainingDeclension' => 'тренировок'
    ],
    [
      'id' => 'beginner',
      'name' => 'Интенсивный',
      'price' => '2&nbsp;990 рублей',
      'description' => 'Первый этап на&nbsp;пути к&nbsp;твоему идеальному телу.<br>Изучаем и&nbsp;отрабатываем основные базовые движения.',
      'experience' => 'Опыт тренировок: Отсутствует или менее 6&nbsp;мес.',
      'about' => [
        'Сформируем базовые навыки работы со&nbsp;своим телом;',
        'Начнем налаживать питание;',
        'Подготовимся к&nbsp;работе со&nbsp;свободными весами;'
      ],
      'time' => '1 неделя',
      'numberOfTrainings' => '5',
      'trainingDeclension' => 'тренировок'
    ],
    [
      'id' => 'beginner',
      'name' => 'Продвинутый',
      'price' => '2&nbsp;990 рублей',
      'description' => 'Первый этап на&nbsp;пути к&nbsp;твоему идеальному телу.<br>Изучаем и&nbsp;отрабатываем основные базовые движения.',
      'experience' => 'Опыт тренировок: Отсутствует или менее 6&nbsp;мес.',
      'about' => [
        'Сформируем базовые навыки работы со&nbsp;своим телом;',
        'Начнем налаживать питание;',
        'Подготовимся к&nbsp;работе со&nbsp;свободными весами;'
      ],
      'time' => '2 недели',
      'numberOfTrainings' => '15',
      'trainingDeclension' => 'тренировок'
    ],
    [
      'id' => 'beginner',
      'name' => 'Хардкор',
      'price' => '2&nbsp;990 рублей',
      'description' => 'Первый этап на&nbsp;пути к&nbsp;твоему идеальному телу.<br>Изучаем и&nbsp;отрабатываем основные базовые движения.',
      'experience' => 'Опыт тренировок: Отсутствует или менее 6&nbsp;мес.',
      'about' => [
        'Сформируем базовые навыки работы со&nbsp;своим телом;',
        'Начнем налаживать питание;',
        'Подготовимся к&nbsp;работе со&nbsp;свободными весами;'
      ],
      'time' => '4 недели',
      'numberOfTrainings' => '12',
      'trainingDeclension' => 'тренировок'
    ],
  ];
  
  ?>
  <section id="levels">
      <div class="container">
        <div class="levels">
          <h2 class="levels__head">Выбери свой <span>уровень:</span></h2>

          <div class="levels__btns">
            <div class="levels__btns-control">
              <div class="levels__btns-control-prev"></div>
              <div class="levels__btns-control-next"></div>
            </div>
            <div class="levels__btns-carousel">
            <?php foreach($trainingLevels as $key => $levelInfo):?> <?php $key = $key +1;?>
                <div class="levels__btn-wrap">
                  <button class="levels__btn <?= ($key == 1) ? 'active' : '' ?>" data-tab-link="tab-<?=$key?>"><?=$levelInfo['name']?></button>
                </div>
            <? endforeach;?>
            </div>
          </div>

          <div class="levels__tabs">
          <?php foreach($trainingLevels as $key => $levelInfo):?> <?php $key = $key +1;?>
              <div class="tab <?= ($key == 1) ? 'show-tab' : '' ?>" data-tab-content="tab-<?=$key?>">
                <div class="tab__picture">
                  <span class="tab__circle"></span>
                  <img class="tab__img animated fadeInLeft" src="/assets/img/tab-<?=$key?>.png" alt="img">
                </div>

                <div class="tab__description-wrap">
                  <div class="tab__description">
                    <div class="tab__description-time">
                      <div class="tab-num">
                        <span class="tab-num__num animated flipInX"><?=$levelInfo['numberOfTrainings']?></span>
                        <span class="tab-num__text"><?=$levelInfo['trainingDeclension']?></span>
                      </div>
                      <div class="tab-week">
                        <span class="tab-week__text animated fadeIn delay-0 5s"><?=$levelInfo['time']?></span>
                      </div>
                    </div>
                    <div class="tab__description-text">
                      <div class="tab__text">
                        <?=$levelInfo['description']?>
                      </div>
                      <div class="tab-experience">
                        <span class="tab-experience__text"><?=$levelInfo['experience']?></span>
                      </div>
                      <ul class="tab-inner-list">
                        <?php foreach($levelInfo['about'] as $aboutText):?>
                          <li class="tab-inner-list__item">&bull; <?=$aboutText?></li>
                        <? endforeach;?>
                      </ul>
                    </div>
                  </div>

                  <div class="tab-btns">
                    <div class="tab-btns__btn tab-btns__btn-price"> <?=$levelInfo['price']?></div>
                    <button class="tab-btns__btn tab-btns__btn-learn" data-popup="open" data-id="course-info-<?=$levelInfo['id']?>">Подробнее об уровне</button>
                    <button class="tab-btns__btn tab-btns__btn-buy" data-popup="open" data-id="buy-course-<?=$levelInfo['id']?>">Купить</button>
                  </div>
                </div>

                <div class="tab__line"></div>
              </div>
              <? endforeach;?>
          </div>
        </div>
      </div>
    </section>

  <?php
  # Чисто как загрушка, вставить данные
  $feedbacks = [
    [
      'title' => 'Test 1',
      'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, harum?',
    ],
    [
      'title' => 'Test 2',
      'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, harum?',
    ],
    [
      'title' => 'Test 3',
      'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, harum?',
    ],
    [
      'title' => 'Test 4',
      'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, harum?',
    ],
  ];
  #///
  ?>

  <section id="reviews">
      <div class="container">
        <div class="reviews">
          <h2 class="reviews__head">ОТЗЫВЫ:</h2>
          <div class="reviews__body">
            <div class="reviews-carousel">
              <?php foreach($feedbacks as $feed):?>
                <div class="reviews-slide">
                  <div class="reviews-slide-wrap">
                    <div class="reviews-slide__avatar-wrap">
                      <div class="reviews-slide__avatar" style="background-image: url(/assets/img/VRM43wI5vPk.jpg);"></div>
                    </div>
                    <div class="reviews-slide__content">
                      <h5 class="reviews-slide__head"><?=$feed['title']?></h5>
                      <div class="reviews-slide__text">
                          <?=$feed['text']?>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach;?>
            </div>
          </div>
          
          <div class="decor-wrap">
            <img class="decor-img" src="/assets/img/f_screen_icon1.png" alt="img">
            <span class="decor-span"></span>
          </div>

        </div>
      </div>
    </section>
  
  
  




<?php
  include('footer.php');
?>