<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stage площадка</title>
    <link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/boostrap/css/bootstrap.min.css">
    <script src="/boostrap/js/jquery-3.5.1.min.js"></script>
    <script src="/boostrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/tiny-slider.js"></script>
    <link rel="icon" href="/assets/img/icons/icon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">
</head>
<body>
    <div class="main-container">